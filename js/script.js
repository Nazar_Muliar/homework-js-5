function deepClone(obj) {
    let newObj = {};
    for (let propertyName in obj) {
        if(typeof(propertyName) != "object") {
            newObj[propertyName] = obj[propertyName];
        }
        else {
            deepClone(newObj[propertyName]);
        }
    }
    return newObj;
    /*
    1. Проверить, что храниться в очередном свойстве - простое значение или объект
    2. Если простое значение - скопировать через =
    3. Если объект - свойству присваивается пустой объект и мы заново вызываем функцию глубого копирования
     */


}


let firstPerson = {
    name: "Кристофер",
    "full name": {
        "first name": "Кристофер",
        "last name": "Робин"
    }
}

let newObj=deepClone(firstPerson);
console.log(newObj);
